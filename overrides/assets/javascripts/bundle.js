// ---------- 自訂元素(print換頁) ----------
class NewPageElement extends HTMLElement {
  constructor() {
    super();
  }
}
customElements.define("new-page", NewPageElement);



window.onload = function () {

  // -------table code-----------

  let fileLoadingPromises = [];

  let codes = document.querySelectorAll('.code');
  codes.forEach(function (code, index) {
    let codeValue = code.innerHTML.trim();
    let preId = `code_${index}`;

    let fileLoadingPromise = fetch(`${codeValue}.txt`)
      .then(response => response.text())
      .then(text => {
        let converter = new showdown.Converter();
        let html = converter.makeHtml(text);
        code.innerHTML = html;
        const pre = code.querySelector('pre');
        pre.id = preId;
        pre.innerHTML += `<button class="md-clipboard md-icon" title="Copy to clipboard" data-clipboard-target="#${preId}"></button>`
      })
      .catch(error => console.error('Error:', error));

    fileLoadingPromises.push(fileLoadingPromise);
  });

  // -----------svg-------------

  let svgConversionPromises = [];

  let lightboxLinks = document.querySelectorAll('.glightbox');
  lightboxLinks.forEach(function (lightboxLink) {
    let imgElement = lightboxLink.querySelector('img');
    if (imgElement.src && imgElement.src.endsWith('.svg')) {
      let xhr = new XMLHttpRequest();
      xhr.open('GET', imgElement.src, true);

      let svgConversionPromise = new Promise(function (resolve, reject) {
        xhr.onreadystatechange = function () {
          if (xhr.readyState === 4 && xhr.status === 200) {
            let divElement = document.createElement('div');
            divElement.classList.add('img-to-svg');
            let svgContent = xhr.responseText;
            lightboxLink.replaceWith(divElement);
            divElement.innerHTML = svgContent;
            resolve();
          }
        };

      });

      svgConversionPromises.push(svgConversionPromise);

      xhr.send();
    }
  });

  Promise.all([...fileLoadingPromises, ...svgConversionPromises]).then(function () {
    let script = document.createElement('script');
    script.src = 'https://unpkg.com/@highlightjs/cdn-assets@11.9.0/highlight.min.js';
    script.onload = function () {
      window.hljs.initHighlightingOnLoad();
      // ----------print-----------
      if (window.location.href.indexOf("/print_page") > -1) {
        window.print();
      }
    };
    document.body.appendChild(script);
  });


  // -----------breadcrumb----------

  const mdNavPrimary = document.querySelector(".md-sidebar__scrollwrap");
  const links = mdNavPrimary.querySelectorAll(".md-nav__list a");

  let breadcrumb = document.querySelector(".breadcrumb");

  let homeLink = document.createElement("a");
  homeLink.setAttribute("href", "/");
  homeLink.textContent = "Home";
  breadcrumb.appendChild(homeLink);

  const currentPagePath = window.location.pathname;
  let lastMatchedLink = null;
  links.forEach(function (link) {
    if (link.href.indexOf("#") === -1) {
      const linkPath = link.pathname;
      if (currentPagePath.startsWith(linkPath)) {
        let separator = document.createElement("span");
        separator.textContent = " > ";
        breadcrumb.appendChild(separator);
        let clonedLink = document.createElement("a");
        clonedLink.setAttribute("href", link.getAttribute("href"));
        clonedLink.innerHTML = link.innerHTML;
        breadcrumb.appendChild(clonedLink);
        lastMatchedLink = clonedLink;
      }
    }
  });
  if (lastMatchedLink !== null) {
    let lastLinkText = lastMatchedLink.textContent;
    breadcrumb.removeChild(lastMatchedLink);
    let extraContent = document.createTextNode(lastLinkText);
    breadcrumb.appendChild(extraContent);
  }

  breadcrumb.scrollLeft = breadcrumb.scrollWidth;
}